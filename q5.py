"""
This module contains functions for decoding messages.
"""

import base64

def decode_message_1():
    """
    The function decodes a hexadecimal message into a string using the ISO-8859-1 encoding.
    """
    msg = """
    4573746520657320656c20fa6c74696d6f207061736f2c20706f72206661766f722c20616772
    6567616d6520616c2068616e676f75743a200d0a0d0a226d617274696e406d656e646f7a6164656c
    736f6c61722e636f6d22207061726120736162657220717565206c6c656761737465206120657374
    612070617274652e0d0a0d0a477261636961732c20792065737065726f20766572746520706f7220
    617175ed212e
    """
    return bytes.fromhex(msg).decode('ISO-8859-1')

def decode_message_2():
    """
    The function decodes a base64 encoded message and returns the decoded message in ASCII format.
    """
    msg = """
        U2kgbGxlZ2FzIGhhc3RhIGVzdGEgcGFydGUsIHBvciBmYXZvciwgY29tZW50YW1lbG8gY29uIHVu
        IG1lbnNhamUu
    """
    return base64.b64decode(msg).decode('ascii')

if __name__ == '__main__':
    print(decode_message_1())
    print(decode_message_2())
