"""
Fibonnacci Test Module
"""

import unittest

def fibonacci(number):
    """
    The function calculates the nth number in the Fibonacci sequence using recursion.
    """
    if number == 0:
        return 0
    if number == 1:
        return 1
    return fibonacci(number-1) + fibonacci(number-2)

class TestFibonacci(unittest.TestCase):
    """
    Fibonnacci Test
    """
    def test_fibonacci_zero(self):
        """
        This is a unit test for the `fibonacci` function that checks if it returns 0 when
        passed 0 as an argument.
        """
        self.assertEqual(fibonacci(0), 0)

    def test_fibonacci_one(self):
        """
        This is a unit test for the `fibonacci` function that checks if it returns 1 when
        passed 1 as an argument.
        """
        self.assertEqual(fibonacci(1), 1)

    def test_fibonacci_small(self):
        """
        The function contains test cases for a Fibonacci sequence generator, testing for
        correct output for small input values.
        """
        self.assertEqual(fibonacci(2), 1)
        self.assertEqual(fibonacci(3), 2)
        self.assertEqual(fibonacci(4), 3)

    def test_fibonacci_large(self):
        """
        The function contains test cases for a Fibonacci sequence generator, testing for
        correct output for large input values.
        """
        self.assertEqual(fibonacci(10), 55)
        self.assertEqual(fibonacci(20), 6765)

if __name__ == '__main__':
    unittest.main()
