""" SEE q4.md for details """

def binary_search(lst: list, target: int) -> int:
    """
    The binary_search function takes a sorted list and a target integer as input, and returns
    the index of the target integer in the list using the binary search algorithm.
    """
    left, right = 0, len(lst) - 1
    while left <= right:
        mid = (left + right) // 2
        if lst[mid] == target:
            return mid
        if lst[mid] < target:
            left = mid + 1
        else:
            right = mid - 1
    return -1

def find_pair(lst: list, target: int) -> str:
    """
    The function takes a list of integers and a target integer, and returns a string
    indicating whether there exists a pair of integers in the list that add up to the target.
    """
    for i, num in enumerate(lst):
        complement = target - num
        j = binary_search(lst[i+1:], complement)
        if j != -1:
            return f'Yes, matching pair ({num},{lst[j+i+1]})'
    return 'Nop'


if __name__ == '__main__':
    TARGET = 8

    numbers = [1, 3, 6, 7]
    print(find_pair(numbers, TARGET)) # OK, matching pair (1,7)

    numbers = [2, 3, 3, 7]
    print(find_pair(numbers, TARGET)) # No
