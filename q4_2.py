""" SEE q4.md for details """

def find_pair(lst: list, target: int) -> str:
    """
    The function takes a list of integers and a target integer, and returns a string
    indicating whether there exists a pair of integers in the list that add up to the target.
    """
    left, right = 0, len(lst) - 1
    while left < right:
        total = lst[left] + lst[right]
        if total == target:
            return f'OK, matching pair ({lst[left]},{lst[right]})'
        if total < target:
            left += 1
        else:
            right -= 1
    return 'No'


if __name__ == '__main__':
    TARGET = 9

    numbers = [2, 3, 6, 7]
    print(find_pair(numbers, TARGET)) # OK, matching pair (2,7)

    numbers = [1, 3, 3, 7]
    print(find_pair(numbers, TARGET)) # No
