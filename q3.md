# Projects

1. [Lazaro Estrada](https://lazaroestrada.com) (Personal Website/Porfolio)

2. [Fizami](https://fizami.com) (Personal Project Under Construction - Outsourcing Company)

3. [NordSyd](https://nordsyd.com) - (Personal Project Under Construction - A free site for companies to post jobs and professionals to search and apply for jobs, backend built with Python, Django, Django REST Framework, PostgreSQL, AWS (RDS, S3, EC2, Elastic Beanstalk) and frontend built with React, NextJS, AWS Amplify, GitHub Actions)

> NOTE: I have worked on multiple projects using the required technologies, but for confidentiality reasons I cannot share much information about it.
